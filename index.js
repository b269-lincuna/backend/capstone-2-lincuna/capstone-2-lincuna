/*	Capstone 2 - E-commerce 
	API - Requirements and 
	Model Design
*/

//E-commerce API MVP requirements

/*	Minimum Requirements
		1.) User registration
		2.) User authentication
		3.) Create Product (Admin only)
		4.) Retrieve all active products
		5.) Retrieve single product
		6.) Update Product information (Admin only)
		7.) Archive Product (Admin only)
		8.) Non-admin User checkout (Create Order)
		9.) Retrieve User Details

	Stretch Goal
		10.) Set user as admin (Admin only)
		11.) Retrieve authenticated user’s orders
		12.) Retrieve all orders (Admin only)
		13.) Add to Cart
			13.1.) Added Products
			13.2.) Change product quantities
			13.3.) Remove products from cart
			13.4.) Subtotal for each item
			13.5.) Total price for all items

		[ Insert ]
		14.) Check-out Cart to Orders
		15.) Check-out Single Product on Cart to Order
*/

//Setup dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors"); // (preparation for full-stack dev)

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");
const cartRoute = require("./routes/cartRoute");

// Server Setup
const app = express();

//Middlewares
app.use(cors()); // (preparation for full-stack dev)
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// DATABASE CONNECTION [START] *****************
mongoose.connect("mongodb+srv://jamepaullincuna:admin123@zuitt-bootcamp.k9jjqkm.mongodb.net/eCommerceAPI?retryWrites=true&w=majority",
	{useNewUrlParser: true,useUnifiedTopology: true}
);

mongoose.connection.once("open", () => console.log("Now connected to the cloud database!"));
// DATABASE CONNECTION [ END ] *****************

app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);
app.use("/carts", cartRoute);

app.listen(process.env.PORT || 4000 , () => console.log(`Now listening to port ${process.env.PORT || 4000}.`));
