
const User = require("../models/User");
const Order = require("../models/Order");

// "bcrypt" is a password-hashing function
const bcrypt = require("bcrypt");
const auth = require("../auth");

//route for checking email
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then( result => (result.length > 0));
};

// Retreive user details
module.exports.getProfile2 = (userData) => {
	return User.findById(userData.id).then(result => {
		if (result == null) {
			return false
		} else {
			result.password = "*****"
			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};

// update user details
module.exports.updateUserDetails = (data) => {
	return User.findByIdAndUpdate(data.userId,{
		firstName:data.reqBody.firstName,
		lastName:data.reqBody.lastName,
		//password:bcrypt.hashSync(data.reqBody.password,10),
		isAdmin:data.reqBody.isAdmin
		})
	.then( (user,error) => !error);

	/*return User.findById(data.userId).then(result => {
		if (result == null) {
			return false
		} else {
			// Returns the user information with the password as an empty string or asterisk.
			return result.save()
		}
	})*/
};

// 1.) User registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName : reqBody.firstName,
		lastName  : reqBody.lastName,
		email 	  : reqBody.email,	
		password  : bcrypt.hashSync(reqBody.password,10)
		// "bcrypt.hashSync" is function in the bcrypt library that used to generate hash value for a given input string synchonously
		// "reqBody.password" - input string that need to hash
		// 10 - user defined value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt password
	});
	return newUser.save().then( (user,error) => !error )
	
};

// 2.) User authentication
module.exports.loginUser = (reqBody) => {
	let message;
	return User.findOne({email: reqBody.email}).then(result=>{
		if (result == null){
			//message = Promise.resolve("Account Not exist!");
			return false;
		} else {
			// Checking if field email and password exist in reqBody
			if ( reqBody.hasOwnProperty('password')) {
				// Checks if password matches
				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

				if(isPasswordCorrect){
					message = { 
						status : `Welcome ${result.firstName} ${result.lastName}!`,
						access : auth.createAccessToken(result)
					};
					//return true;
					return message;
				} else { 
					return false;
					//message = Promise.resolve("Password is incorrect!");
				}
			} else {
				return false;
				//message = Promise.resolve("Please input password!");
			}
		}
		//return message.then((value)=>value);
	});
};

// 9.1) Retrieve User Details by userId
module.exports.getUserDetails=(data)=>{
	if (data.authorization.isAdmin) { // Admin can view any user details
		return User.findById(data.userId).then(result => result);
	} else { // User can only view its own details
		return User.findById(data.authorization.id).then(result => result);
	}
};

// 9.2) Retrieve All User Details (Admin Only)
module.exports.getAllUsers=(isAdmin)=>{
	if (isAdmin) {
		return User.find({}).then(result => result);
	} 

	let message = Promise.resolve("User must be ADMIN to access this!");
	return message.then((value)=>value);
};

// 10.) [STRETCH GOAL] Set user as admin (Admin only) 
module.exports.updateUserRole=(data)=>{
	if (data.isAdmin) {
		return User.findByIdAndUpdate(data.userId,{isAdmin:data.userRole})
		.then( (user,error) => !error);
	}

	let message = Promise.resolve("User must be an ADMIN to access this!");
	return message.then((value)=>value);
};

// 11.) [STRETCH GOAL] Retrieve authenticated user’s orders
module.exports.getUserOrder=(data)=>{
	if (data.authorization.isAdmin) { //if admin, it can view orders of specific user
		return Order.find({userId : data.userId}).then(result => result);
	} else { //if not admin, auth user can only view their own orders
		/* ELSE will only show orders base on its own ACCESS ID 
		EVEN IF the user input other USER'S ID AS URL PARAMETER */
		return Order.find({userId : data.authorization.id}).then(result => result);	
	}
};

// 12.) Retrieve all orders (Admin only)
module.exports.showAllOrders=(isAdmin)=>{
	if (isAdmin) {
		return Order.find({}).then(result => result);
	}
	let message = Promise.resolve("User must be ADMIN to access this!");
	return message.then((value)=>value);
};
