
const Product = require("../models/Product");
const Order = require("../models/Order");

// require a mongoose function for validation an objectId
// isValidObjectId(<objectId>) returns true if valid, false if not.
const {isValidObjectId} = require('mongoose'); 

// 8.) Non-admin User checkout (Create Order)
module.exports.createOrder = async (data) => {

	// Checking if User is not an Admin
	if (!data.authorization.isAdmin) {		
		
		/* Getting products data inside Data.Order and Merge the data of ProductDetails 
		and assign it to orderedProducts variable */
		let orderedProducts = [];

		// We use this 'for...of' loopings so we can break the loop if productId does not exist
		// forEach cannot be break unless you throw an exception or an error(using try catch block)
		for (const product of data.order.products) {
			// 'product' handles element one a time in every iteration on 'data.order.products' array.

			/* Handles Invalid objectId before using it in query because if an objectId is not valid, an error will occur and query will not execute properly */
			if (!isValidObjectId(product.productId)) {
				// let message = `ProductID : [${product.productId}] is not valid. Please check the productId carefully.`;
				// return Promise.resolve(message);
				return false;
			}

			let productDetails = await Product.findById(product.productId).then(result => result);
			/* if (productDetails) is not a boolean condition, but it checks if it has a truthy value that means all value it true unless its not null, undefined, false, 0, an empty string, or NaN [Not-a-Number]*/
			if (productDetails) {
				let productSummary = {
					productId 	: product.productId,
					name 		: productDetails.name,
					description : productDetails.description,
					price 		: productDetails.price,
					quantity 	: product.quantity
				};
				orderedProducts.push(productSummary);
			} else {
				/* Handles Valid ID but not exist in Products collection*/
				// let message = `ProductID : [${product.productId}] does not exist. Please check the productId carefully.`;
				// return Promise.resolve(message);
				return false;
			}
		}

		// Getting the amount of products checkedOut using reduce() method
		const productTotalPrice = orderedProducts.reduce((accumulator, objData) => {
			return accumulator + (objData.price * objData.quantity) ;
		}, 0);

		// Creating New Order
		let newOrder = new Order ({
			userId 		: data.authorization.id,
			products 	: orderedProducts,
			totalAmount : productTotalPrice
		});

		// returns true if New Order is created/saved, false if not.
		return await newOrder.save().then((orderData, error ) => !error);
		
	}

	// if User is an Admin, send a message and resolve promise to return to function module.
	// let message = Promise.resolve("Only User can check out products!");
	// return message.then((value)=>value);
	return false;
};