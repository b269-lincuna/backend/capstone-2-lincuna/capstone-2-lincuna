
const Product = require("../models/Product");
const Order = require("../models/Order");
const Cart = require("../models/Cart");

// require a mongoose function for validation an objectId
// isValidObjectId(<objectId>) returns true if valid, false if not.
const {isValidObjectId} = require('mongoose');

/*	13.) Add to Cart 							
		13.1.) Added Products
		13.2.) Change product quantities
		13.3.) Remove products from cart
		13.4.) Subtotal for each item
		13.5.) Total price for all items */

/* 1.) route show cart
			if exist showCart
			else message Empty Cart */

// 13.1.) Added Products (Show Cart)
module.exports.showCart = async (data) => {

	// if Admin, showAll cart of specific user, if not show its own cart with placeOrder is NULL
	/*const fCondition = data.authorization.isAdmin ? 
						  { userId: data.userId } : 
						  { userId: data.authorization.id , placeOrderOn:null };*/

	const result =await Cart.findOne({userId: data.authorization.id , placeOrderOn:null})
	return result || false;
	/*return await Cart.findOne({userId: data.authorization.id , placeOrderOn:null}).then(result => result);*/

	/*return Cart.findOne({userId: data.authorization.id , placeOrderOn:null}).then(result => {
		// check if empty cart
		if (result.length==0){
			let message = Promise.resolve(`Cart is empty! Please add a product.`);
			return message.then((value)=>value);
		}
		return result; // if not empty then return cart data
	});*/
};

/*
module.exports.showCart = (data) => {

	// if Admin, showAll cart of specific user, if not show its own cart with placeOrder is NULL
	const fCondition = data.authorization.isAdmin ? 
						  { userId: data.userId } : 
						  { userId: data.authorization.id , placeOrderOn:null };

	return Cart.find(fCondition).then(result => {
		// check if empty cart
		if (result.length==0){
			let message = Promise.resolve(`Cart is empty! Please add a product.`);
			return message.then((value)=>value);
		}
		return result; // if not empty then return cart data
	});
};
*/

module.exports.showAllCart = (data) => {

	// if Admin, showAll cart of specific user, if not show its own cart with placeOrder is NULL
	/*const fCondition = data.authorization.isAdmin ? 
						  { userId: data.userId } : 
						  { userId: data.authorization.id , placeOrderOn:null };*/
	if (data.authorization.isAdmin) {
		return Cart.find({}).then(result => result);
	}
	/*const result =await Cart.findOne({})
	return result || false;*/
	let message = Promise.resolve("User must be ADMIN to access this!");
	return message.then((value)=>value);
	
};

/*
	2.) route Add to cart add product>> 
			check if user has existing cart
				if true	>> 
					updateCart
					show updateCart
					calculate total

				else >> the create NewCart and calculate total
*/

// 13.) Add to Cart				
// 13.4.) Subtotal for each item 	
// 13.5.) Total price for all items	
module.exports.addProductInCart =  ( data ) => {
	// Only users can add to cart
	if (!data.authorization.isAdmin){
		// async makes a function return a Promise. await makes a function wait for a Promise.
		// Check if there is existing cart for specific userId  and not yet placeOrder
		return Cart.find({userId: data.authorization.id , placeOrderOn:null}).then( async result => {
		
			if (result.length == 0){

		// #######################################
		// |  1.0 [START] : check if empty cart  |
		// #######################################
				
			// ##########################################################
			// |  1.1 [START] : setup the details of req.body.products, |
			// |				push it on cartProducts array           |
			// ##########################################################

				let cartProducts = [];

				for (const product of data.reqBody.products) {
					
					// Handles Invalid objectId before using it in query because if an objectId is not valid, an error will occur and query will not execute properly
					if (!isValidObjectId(product.productId)) {
						let message = `Product ID : [${product.productId}] is not valid. Please check the productId carefully.`;
						return Promise.resolve(message);
					}

					let productDetails = await Product.findById(product.productId);

					if (productDetails) {
						let tempProduct = {
							productId 	: product.productId,
							name 		: productDetails.name,
							description : productDetails.description,
							price 		: productDetails.price,
							quantity 	: product.quantity,
							totalAmount : productDetails.price * product.quantity
						};
						cartProducts.push(tempProduct);
					} else {
						// Handles valid objectId but not exist in Products
						let message = `Product ID : [${product.productId}] does not exist. Please check the productId carefully.`;
						return Promise.resolve(message);
					}
				}

			// #################
			// |  1.1 [ END ]  |
			// #################

			// #################################################################
			// |  1.2 [START] : - Setup the details of cartProducts to newCart |
			// |                - Saving the newCart to Carts collection       |
			// #################################################################

				const cartProductTotalPrice = cartProducts.reduce((accumulator, objData) => {
					return accumulator + (objData.totalAmount) ;
				}, 0);
				
				// SET New Cart Document
				let newCart = new Cart ({
					userId 			: data.authorization.id,
					products 		: cartProducts,
					cartTotalAmount : cartProductTotalPrice
				});

				// Saving New Cart Document
				return newCart.save().then((cData, error ) => !error);

			// #################
			// |  1.2 [ END ]  |
			// #################  

		// #################
		// |  1.0 [ END ]  |
		// #################

			}
			
			else {

		// ############################################################
		// |  2.0 [START] : check if NOT EMPTY then update cart data  |
		// ############################################################

			// ########################################################################################
			// |  2.1 [START] : setup the details of req.body.products and push it on tempCart array  |
			// ########################################################################################

				let tempCart = [];

				for (const product of data.reqBody.products) {
					
					// Handles Invalid objectId before using it in query because if an objectId is not valid, an error will occur and query will not execute properly
					if (!isValidObjectId(product.productId)) {
						let message = `ProductID : [${product.productId}] is not valid. Please check the productId carefully.`;
						return Promise.resolve(message);
					}

					let productDetails = await Product.findById(product.productId);

					// 'if (productDetails)' is not a boolean condition, but it checks if it has a truthy value that means all value it true unless its not null, undefined, false, 0, an empty string, or NaN [Not-a-Number]
					if (productDetails) {
						let tempProduct = {
							productId 	: product.productId,
							name 		: productDetails.name,
							description : productDetails.description,
							price 		: productDetails.price,
							quantity 	: product.quantity,
							totalAmount : productDetails.price * product.quantity,
							addToCartOn : new Date()
						};
						tempCart.push(tempProduct);

					} else { // Handles valid objectId but not exist in products collection

						let message = `ProductID : [${product.productId}] does not exist. Please check the productId carefully.`;
						return Promise.resolve(message);
					}
				}

			// #################
			// |  2.1 [ END ]  |
			// #################
			
			// #####################################################################################
			// |  2.2 [START] : Getting the cart data on database base on authId and placeOrderOn  |
			// |                (if has), then comparing both the tempCart(reqBody.products) VS    |
			// |                the cartProducts in the database                                   |
			// #####################################################################################
				
			    // if products in Old cart data is not exist in new cart data, 
			    // push oldCartData.product to tempCart.products
			    await Cart.findOne({userId:data.authorization.id, placeOrderOn:null}).then( cart=>{
			    	
			    	//'cart' is the existing cart product data in DB base on UserId and placeOrderOn
			    	cart.products.forEach( (cartData)=>{ //array of objects

			    		//checks and set the index if productId of DB productData exist in reqBody productData
			    		let index = tempCart.findIndex(p => p.productId === cartData.productId);
			    		
			    		// findIndex() method returns -1 if it doesn't find an object that satisfies the condition.
			    		if (index === -1) { 
			    		// cartData.productId not exist in tempCart
			    			let oldCartData = {
			    				productId 	: cartData.productId,
			    				name 		: cartData.name,
			    				description : cartData.description,
			    				price 		: cartData.price,
			    				quantity 	: cartData.quantity,
			    				totalAmount : cartData.price * cartData.quantity,
			    				addToCartOn : cartData.addToCartOn
			    			};
			    			tempCart.push(oldCartData);
			    		} else {
			    			//if productId exist in both reqBody cartData and cartData on DB, add quantity of both
			    			tempCart[index].quantity += cartData.quantity;
			    			tempCart[index].totalAmount = tempCart[index].quantity * tempCart[index].price;
			    			tempCart[index].addToCartOn = new Date();
			    		}
			    	});
			    });

		    // #################
		    // |  2.2 [ END ]  |
		    // #################

		    // ###############################################################################
		    // |  2.3 [START] : - Setup the total amount and insert the updated product data | 
		    // |                  to variable updateUserCart                                 |
		    // |				- Updating carts collection									 |
		    // ###############################################################################
			    
			    //just setting to the proper variable name
			    let cartUpdatedProducts = tempCart;

			    //calculate totalAmount of all products in cart using reduce() method
			    const cartTotalPrice = cartUpdatedProducts.reduce((accumulator, objData) => {
			    	return accumulator + (objData.totalAmount) ;
			    }, 0);

			    // Setting Cart Document Fields
			    let updateUserCart = { 
			    	userId 			: data.authorization.id,
			    	products 		: cartUpdatedProducts,
			    	cartTotalAmount : cartTotalPrice 
			    };

			    // Updating Cart Document
			    // condition : userId and placeOrderOn
			    return Cart.findOneAndUpdate({userId: data.authorization.id , placeOrderOn:null}, updateUserCart).then((cData,error) => !error);

		    // #################
		    // |  2.3 [ END ]  |
		    // #################

		// #################
		// |  2.0 [ END ]  |
		// #################

			}

		}); 
	} 

	let message = Promise.resolve("Admin cannot add to cart.");
	return message.then((value) => value);
};

/*
3.) route UpdateCart >> QUANTITY >>
		check if product exist in cart
			if TRUE then change product quantity and recalculate totals
			else	message no product exist in your cart
*/

// 13.2.) Change product quantities
// 13.4.) Subtotal for each item 	
// 13.5.) Total price for all items	
// Update Product Quantity
module.exports.updProdQnty = async (data) => {
	if (!data.authorization.isAdmin) {

		return await Cart.findOne({userId:data.authorization.id, placeOrderOn:null}).then( cart =>{
			
			let tempCart = cart.products;

			//checks and set the index if productId of DB cartData exist in reqBody cartData
			let index = tempCart.findIndex(p => p.productId === data.productId);
			
			// findIndex() method returns -1 if it doesn't find an object that satisfies the condition.
			if (index !== -1) { 
				//if productId exist ,update quantity base on index
				tempCart[index].quantity = data.newQnty;
				tempCart[index].totalAmount = tempCart[index].quantity * tempCart[index].price;
				tempCart[index].addToCartOn = new Date();
			
				let cartUpdatedProducts = tempCart;

				// re-calculate totals
				const cartTotalPrice = cartUpdatedProducts.reduce((accumulator, objData) => {
					return accumulator + (objData.totalAmount) ;
				}, 0);

				// Setting Cart Document
			    let updateUserCart = { 
			    	userId 			: data.authorization.id,
			    	products 		: cartUpdatedProducts,
			    	cartTotalAmount : cartTotalPrice 
			    };
				
				return Cart.findOneAndUpdate({userId: data.authorization.id , placeOrderOn:null}, updateUserCart).then((result,error) => !error);

			} else {
				let message = Promise.resolve("Product does not exist in your cart!");
				return message.then( (value) => value );
			}
		});
	}

	let message = Promise.resolve("Admin is not allowed to access this!");
	return message.then( (value) => value );
};

/*
4.) route removeProduct on Cart >> DELETE PRODUCT >>
		check if product exist in cart
			if TRUE then remove product and recalculate totals
			else	message no product exist in your cart
*/

// 13.3.) Remove products from cart 
// 13.4.) Subtotal for each item 	
// 13.5.) Total price for all items				   
module.exports.rmvFromCart = async (data) => {
	if (!data.authorization.isAdmin) {

		return await Cart.findOne({userId:data.authorization.id, placeOrderOn:null}).then( cart=>{
			
			let tempCart = cart.products;
			
			//checks and set the index if productId of DB cartData exist in reqBody cartData
			let index = tempCart.findIndex(p => p.productId === data.productId);
			
			// findIndex() method returns -1 if it doesn't find an object that satisfies the condition.
			if (index !== -1) { 
				
				tempCart.splice(index,1); //removes object inside array base on index
				let cartUpdatedProducts = tempCart;

				// re-calculate totals
				const cartTotalPrice = cartUpdatedProducts.reduce((accumulator, objData) => {
					return accumulator + (objData.totalAmount) ;
				}, 0);

				// Setting Cart Document
			    let updateUserCart = { 
			    	products 		: cartUpdatedProducts,
			    	cartTotalAmount : cartTotalPrice 
			    };
				
				// update Cart
				return Cart.findOneAndUpdate({userId: data.authorization.id , placeOrderOn:null}, updateUserCart).then((result,error) => !error);

			} else {
				let message = Promise.resolve("Product does not exist in your cart!");
				return message.then( (value) => value );
			}
		});
	}
	let message = Promise.resolve("Admin is not allowed to access this!");
	return message.then( (value) => value );
};


/*
5.) route checkOutCart >>
		add Carts Document in Orders collection
		and set Date placeOrderOn
*/

// 14.) Check-out Cart to Order 
module.exports.checkOutCart = (data) => {

	if (!data.authorization.isAdmin) {
		
		// Handles Invalid objectId before using it in query because if an objectId is not valid, an error will occur and query will not execute properly
		if (!isValidObjectId(data.cartId)) {
			let message = `Cart ID : [${data.cartId}] is not valid. Please check it carefully.`;
			return Promise.resolve(message);
		}

		return Cart.findOne({
			_id	   		 : data.cartId,
			userId 		 : data.authorization.id,
			placeOrderOn : null
		}).then((cart) => {

			if ( cart !== null ) { // if a document found / exist
				
				let cartProducts = cart.products.map( (product) => {
					let productDetails = {
						productId 	: product.productId,
						name 		: product.name,
						description : product.description,
						price 		: product.price,
						quantity 	: product.quantity
					};
			    	return productDetails;
			    });

				let newOrder = new Order ({ 
						userId 		: data.authorization.id,
						cartId 		: data.cartId,
						products 	: cartProducts,
						totalAmount : cart.cartTotalAmount,
						purchasedOn : new Date()
				});

				// Saving New Order
				let isOrderSaved = newOrder.save().then((order, error ) => !error);

				// Updating Cart by id
				let isCartCheckOut = Cart.findByIdAndUpdate(data.cartId, { placeOrderOn : new Date() })
				.then((fCart,error) => !error);

				if ( isOrderSaved && isCartCheckOut ) { 
					// if order is saved and cart placeOrderOn updated
					let message = Promise.resolve("Checked-Out Successfully!");
					return message.then((value)=>value);
				} else {
					// if an saving document failed then message the user
					let message = Promise.resolve("Cart Checking-Out Failed! Please report to the administrator.");
					return message.then((value)=>value);
				}
			} else {
				// if no document found, message the user
				let message = Promise.resolve("No cart data found!");
				return message.then((value)=>value);
			}
		});
		
	}
	// if user is an admin
	let message = Promise.resolve("Admin cannot access this!");
	return message.then((value)=>value);
};

// to be continue
// 15.) Check-out Single Product in Cart to Order 
module.exports.checkOutSingleProduct = (data) => {

	// Check if user only
	if (!data.authorization.isAdmin) {

	// #######################################
	// |  1.0 [START] : check if empty cart  |
	// #######################################

		// Handles Invalid objectId before using it in query because if an objectId is not valid, an error will occur and query will not execute properly
		if (!isValidObjectId(data.cartId)) {
			let message = `Cart ID : [${data.cartId}] is not valid. Please check it carefully.`;
			return Promise.resolve(message);
		}

		if (!isValidObjectId(data.productId)) {
			let message = `Product ID : [${data.productId}] is not valid. Please check it carefully.`;
			return Promise.resolve(message);
		}

	// #################
	// |  1.0 [ END ]  |
	// #################

	// ######################################################################
	// |  2.0 [START] : - Getting Specific Product on Cart  				|
	// |                - Save to New Cart 									|
	// |                - Save New Order base on New Cart 					|
	// |                - Remove Specific Product and Update Old Cart Data  |
	// ######################################################################
	
		// SINCE we are checking-out one product only in the cart, we will create new cart for that specific product to have seperate cartId reference 

		// first get the specific cart and the product 
		return Cart.findOne({
			_id	   		 : data.cartId,
			userId 		 : data.authorization.id,
			placeOrderOn : null
		}).then( async (cart) => {

		// ##################################################
		// |  2.1 [START] : - Check if Cart does not exist	|
		// ##################################################

			// checks if cart is not empty
			if ( cart !== null ) {

				// Set cart.products to cartData variable
				let cartData = cart.products

				//create temporary holder
				let tempCart = [];

			// #########################################################################
			// |  2.1.1 [START] - Find product id Index  			   				   |
			// | 				- Push cart productData in tempcart    				   |
			// | 				- if only 1 data in products, checkOut whole cartData  |
			// #########################################################################

				// get the lenght of products array
				let countProducts = cart.products.length;

				// find the index base on productId
				//checks and set the index if productId URL parameter exist in cartData
				let index = cartData.findIndex(p => p.productId === data.productId);
				
				// findIndex() method returns -1 if it doesn't find an object that satisfies the condition.
				if (index !== -1) { 

				// ###########################################################################
				// |  2.1.1.1 [START] - if only 1 data in products, checkOut whole cartData  |
				// ###########################################################################

					if ( countProducts == 1 ){

						// same function as checkOutCart module
						// if count is 1 , just direct checkOut the whole cart data
						let cartProducts = cart.products.map( (product) => {
							let productDetails = {
								productId 	: product.productId,
								name 		: product.name,
								description : product.description,
								price 		: product.price,
								quantity 	: product.quantity
							};
					    	return productDetails;
					    });

						let newOrder = new Order ({ 
								userId 		: data.authorization.id,
								cartId 		: data.cartId,
								products 	: cartProducts,
								totalAmount : cart.cartTotalAmount,
								purchasedOn : new Date()
						});

						// Saving New Order
						let isOrderSaved = newOrder.save().then((order, error ) => !error);

						// Updating Cart by id
						let isCartCheckOut = Cart.findByIdAndUpdate(data.cartId, { placeOrderOn : new Date() })
						.then((fCart,error) => !error);

						if ( isOrderSaved && isCartCheckOut ) { 
							// if order is saved and cart placeOrderOn updated
							let message = Promise.resolve("Checked-Out Successfully!");
							return message.then((value)=>value);
						} else {
							// if an saving document failed then message the user
							let message = Promise.resolve("Cart Checking-Out Failed! Please report to the administrator.");
							return message.then((value)=>value);
						}

					} 

				// #####################
				// |  2.1.1.1 [ END ]  |
				// #####################

				// ##################################################################################
				// |  2.1.1.2 [START] - if has 2 or more products, get the specific product detail  |
				// ##################################################################################

					else {

						// data.productId exist in cartData
						let getCartData = {
							productId 	: cartData[index].productId,
							name 		: cartData[index].name,
							description : cartData[index].description,
							price 		: cartData[index].price,
							quantity 	: cartData[index].quantity,
							totalAmount : cartData[index].totalAmount,
							addToCartOn : cartData[index].addToCartOn
						};
						tempCart.push(getCartData);
					}

				// #####################
				// |  2.1.1.1 [ END ]  |
				// #####################		


				} else { // if product id not exist, 
					let message = Promise.resolve("Product not found in your cart!");
					return message.then((value)=>value);
				}

			// ###################
			// |  2.1.1 [ END ]  |
			// ###################

			// #############################################
			// |  2.1.2 [START] - Set and Save New Cart    |
			// | 				- Set and Save New Order   |
			// |                - Calculate Total Price    |
			// #############################################

				// Calculate totals
				const cartTotalPrice = tempCart.reduce((accumulator, objData) => {
					return accumulator + (objData.totalAmount) ;
				}, 0);

				// Set New Cart
				let newCart = new Cart ({ 
					userId 			: cart.userId,
					products 		: tempCart,
					// same with totalAmount since single product only
					// if multiple selected product, it must be recalculated
					cartTotalAmount : cartTotalPrice, 
					cartDateOn 		: cart.cartDateOn,
					placeOrderOn	: new Date() //set date since this data will be checkOut to Orders
				});

				// Saving New Cart and New Order
				let isCartOrderSaved = await newCart.save().then((dbCartData, error ) => {
					if (error) { return false; }
					else {

						// Set specific product to be checkOut from carts to orders
						let newProductData = {
							productId 	: cartData[index].productId,
							name 		: cartData[index].name,
							description : cartData[index].description,
							price 		: cartData[index].price,
							quantity 	: cartData[index].quantity
						};

						// Set New Order and set cartId Reference
						let newOrder = new Order ({ 
							userId 		: dbCartData.userId,
							cartId 		: dbCartData._id,
							products 	: newProductData,
							totalAmount : cartTotalPrice
						});

						// Save New Order
						return newOrder.save().then((dbOrderData, error ) => !error);
					}
				});

			// ###################
			// |  2.1.2 [ END ]  |
			// ###################
			
			// #############################################
			// |  2.1.3 [START] - Remove Specific Product  |
			// | 				- Re-calculate totals      |
			// |                - Update Old Cart Data     |
			// #############################################

				if (isCartOrderSaved){ // If TRUE then UPDATE OLD CART

					// NOTE: the 'cart' here is the result of the Cart.findOne query in section [2.0]

					// remove productId in products since it is already set to New Cart
					cart.products.splice(index,1);

					// re-calculate totals
					const reCalculateTotal = cart.products.reduce((accumulator, objData) => {
						return accumulator + (objData.totalAmount) ;
					}, 0);

					// set Total Amount
				    cart.cartTotalAmount = reCalculateTotal;
				    
				    // Update Old Cart Data
				    return cart.save().then((cData, error ) => !error);

				} else {
					let message = Promise.resolve("Cart/Order Process Failed!");
					return message.then((value)=>value);
				}

			// ###################
			// |  2.1.3 [ END ]  |
			// ###################

			}

		// #################
		// |  2.1 [ END ]  |
		// #################

		// ######################################
		// |  2.2 [START] - if cartId not exist |
		// ######################################

			// if cart is empty
			else{
				// if no document found, message the user
				let message = Promise.resolve("No cart data found!");
				return message.then((value)=>value);
			}

		// #################
		// |  2.2 [ END ]  |
		// #################

		});

	// #################
	// |  2.0 [ END ]  |
	// #################

	}

	// if user is an admin
	let message = Promise.resolve("Admin cannot access this!");
	return message.then((value)=>value);
};


// 16.) Update cart placeOrderOn to null value (admin only)
module.exports.updatePlaceOrderOn = (data) => {
	if(data.isAdmin){
		return Cart.findByIdAndUpdate(data.cartId, { placeOrderOn : null })
		.then((cart,error) => !error);
	}
	// if user is not an admin
	let message = Promise.resolve("User cannot access this!");
	return message.then((value)=>value);
};