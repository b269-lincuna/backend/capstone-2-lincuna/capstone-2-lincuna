
const Product = require("../models/Product");

//3.) Create Product (Admin only)
module.exports.createProduct = (data) => {
	if (data.isAdmin) {
		let newProduct = new Product ({
			name 		: data.product.name,
			description : data.product.description,
			price 		: data.product.price
		});
		return newProduct.save().then((product, error ) => !error)
	}
	return false;
	/*let message = Promise.resolve("User must be ADMIN to access this!");
	return message.then((value)=>value);*/
};

// 4.) Retrieve all ACTIVE products
module.exports.getAllActiveProduct=()=>{
	return Product.find({isActive: true}).then(result => result);
};

// Retrieve all products
module.exports.getAllProduct=()=>{
	return Product.find({}).then(result => result);
};

// Retrieve all products
module.exports.showProduct=(avail)=>{
	if(avail==1){
		return Product.find({isActive:true}).then(result => result);	
	} else if(avail==2){
		return Product.find({isActive:false}).then(result => result);	
	} else{
		return Product.find({}).then(result => result);	
	}
	
};

// 5.) Retrieve single product
module.exports.getProduct=(productId)=>{
	return Product.findById(productId).then(result => result);
};

// 6.) Update Product information (Admin only)
module.exports.updateProduct = (data) => {
	if (data.isAdmin) {

		// 'delete' is only applicable on objects and arrays
		// removes the product._id field to prevent from updating it
		delete data.product._id; 

		// Mongoose will only update the fields that exist in the document and match the fields in the update object, and will ignore any fields that do not exist.
		return Product.findByIdAndUpdate(data.productId, data.product)
		.then((product,error) => !error);
	}
	return false;
	// let message = Promise.resolve("User must be ADMIN to access this!");
	// return message.then((value)=>value);
};

// 7.) Archive Product (Admin only)
module.exports.archiveProduct = (data) => {
	if (data.isAdmin) {
		//return Product.findByIdAndUpdate(data.productId,{isActive:false})
		return Product.findByIdAndUpdate(data.productId,{ isActive: data.reqBody.isActive })
		.then((product,error) => !error);

	}
	return false;
	/*let message = Promise.resolve("User must be ADMIN to access this!");
	return message.then((value)=>value);*/
};
