
/*  ● User
		○ Email (String)
		○ Password (String)
		○ isAdmin (Boolean - defaults to false)
*/

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName 	 : { // added
		type 	 : String,
		required : [true, "First name is required"]
	},
	lastName 	 : { // added
		type	 : String,
		required : [true, "Last name is required"]
	},
	email 		 : { 
		type 	 : String, 
		required : [true, "Email is required"]
	},
	password 	 : { 
		type 	 : String, 
		required : [true, "Password is required"]
	},
	isAdmin 	 : {	
		type 	 : Boolean, 
		default  : false
	}
});

module.exports = mongoose.model("User",userSchema);
