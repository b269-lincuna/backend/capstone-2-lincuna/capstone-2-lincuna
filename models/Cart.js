/* Order collection
	○ userId (ObjectId)
		■ Must be associated with user who owns the order
	○ products (Array of Objects)
		■ productId (String)
		■ name (String)
		■ description (String)
		■ price (String)
		■ quantity (Number)
		■ totalAmount (Number)
		■ addToCartOn (Date - defaults to current timestamp)
*/

const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
	userId 		 : {
		type  	 : String,
		required : [true, "UserId is required"] 
	},
	products : [
		{
			productId    : {
				type 	 : String,
				required : [true, "Product is required"] 
			},
			name         : {
				type 	 : String,
				required : [true, "Product is required"] 
			},
			description  : {
				type 	 : String,
				required : [true, "Description is required"] 
			},
			price        : {
				type 	 : Number,
				required : [true, "Price is required"] 
			},
			quantity     : {
				type 	 : Number,
				required : [true, "Quantity is required"] 
			},
			totalAmount  : {
				type 	 : Number, 
				default  : 0 
			},
			addToCartOn  : {
				type 	 : Date, 
				default  : new Date() 
			}
		}
	],
	cartTotalAmount : { 
			type    : Number, 
			default : 0
	},
	cartDateOn 		: {
			type 	: Date, 
			default : new Date() 
	},
	placeOrderOn	: { 
			type	: Date, 
			default : null 
	} // where User already place their cart to order
});

module.exports = mongoose.model("Cart", cartSchema);


/*
1.) route show cart
		if exist showCart
		else message Empty Cart

2.) route Add to cart add product>> 
		check if user has existing cart
			if true	>> 
				updateCart
				show updateCart
				calculate total

			else >> the create NewCart and calculate total

3.) route UpdateCart >> QUANTITY >>
		check if product exist in cart
			if TRUE then change product quantity and recalculate totals
			else	message no product exist in your cart

4.) route removeProduct on Cart >> DELETE PRODUCT >>
		check if product exist in cart
			if TRUE then remove product and recalculate totals
			else	message no product exist in your cart

5.) route checkOutCart >>
		addOrder in Orders collection

*/