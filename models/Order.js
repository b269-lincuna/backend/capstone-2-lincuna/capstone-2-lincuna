
/*	● Order
		○ userId (ObjectId)
			■ Must be associated with user who owns the order
		○ products (Array of Objects)
			■ productId (String)
			■ quantity (Number)
		○ totalAmount (Number)
		○ purchasedOn (Date - defaults to current timestamp)
*/

const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId   	 : {
		type 	 : String,
		required : [true, "UserId is required"]
	},
	cartId   	 : { // included for reference if ordered from cart
		type 	 : String,
		default  : null // null if direct order
	},
	products : [
		{
			productId 	 : { 
				type 	 : String,
				required : [true, "Product is required"]
			},
			name 		 : { // included for reference purposes
				type 	 : String,
				required : [true, "Product is required"]
			},
			description  : { // included for reference purposes
				type 	 : String,
				required : [true, "Description is required"]
			},
			price 		 : { // included for reference purposes
				type 	 : Number,
				required : [true, "Price is required"]
			},
			quantity 	 : { 
				type 	 : Number,
				required : [true, "Quantity is required"]
			}
		}
	],
	totalAmount  : { 
		type 	 : Number,
		default  : 0
	},
	purchasedOn  : { 
		type 	 : Date,
		default  : new Date()
	}
});

module.exports = mongoose.model("Order", orderSchema);
