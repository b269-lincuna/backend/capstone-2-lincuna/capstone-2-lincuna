const express = require("express");

// Creates a router instance that functions as middleware and routing system
const router = express.Router();

const orderController = require("../controllers/orderController");
const auth = require("../auth");

// 8.) Non-admin User checkout (Create Order)
router.put("/createOrder", auth.verify, (req,res)=>{
	const data = {
		order 		  : req.body,
		authorization : auth.decode(req.headers.authorization)
	}
	orderController.createOrder(data).then(result => res.send(result));
});

module.exports = router;

