const express = require("express");

// Creates a router instance that functions as middleware and routing system
const router = express.Router();

const productController = require("../controllers/productController");
const auth = require("../auth");

// 3.) Create Product (Admin only)
router.post("/createProduct", auth.verify, (req,res) => {
	const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin	
	} 
	productController.createProduct(data).then(result => res.send(result));
});

// 4.) Retrieve all active products
router.get("/activeProducts",(req,res)=>{
	productController.getAllActiveProduct().then(result => res.send(result));
});

// show all products
router.get("/allProducts",(req,res)=>{
	productController.getAllProduct().then(result => res.send(result));
});

// show products
router.post("/showProducts",(req,res)=>{
	productController.showProduct(req.body.avail).then(result => res.send(result));
});


// 5.) Retrieve single product
router.get("/:productId",(req,res)=>{
	productController.getProduct(req.params.productId).then(result => res.send(result));
});

// 6.) Update Product information (Admin only)
router.post("/updateProduct/:productId", auth.verify, (req,res)=>{
	const data = {
		productId : req.params.productId,
		product   : req.body,
		isAdmin   : auth.decode(req.headers.authorization).isAdmin	
	} 
	productController.updateProduct(data).then(result => res.send(result));
});

// 7.) Archive Product (Admin only)
router.patch("/:productId", auth.verify, (req,res)=>{
	const data = {
		reqBody: req.body,
		productId : req.params.productId,
		isAdmin   : auth.decode(req.headers.authorization).isAdmin	
	} 
	productController.archiveProduct(data).then(result => res.send(result));
});

module.exports = router;