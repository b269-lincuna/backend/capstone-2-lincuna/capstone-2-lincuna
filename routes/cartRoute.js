const express = require("express");

// Creates a router instance that functions as middleware and routing system
const router = express.Router();

const cartController = require("../controllers/cartController");
const auth = require("../auth");

// 13.1.) Added Products (Show Cart)
router.get("/showCart/:userId", auth.verify, (req,res)=>{
	const data = {
		userId        : req.params.userId,
		authorization : auth.decode(req.headers.authorization)
	};
	cartController.showCart(data).then(result => res.send(result));
});

// show all cart
router.get("/showAllCart", auth.verify, (req,res)=>{
	const data = {
		authorization : auth.decode(req.headers.authorization)
	};
	cartController.showAllCart(data).then(result => res.send(result));
});

// 13.) Add to Cart			
// 13.4.) Subtotal for each item 	
// 13.5.) Total price for all items	
router.post("/addToCart", auth.verify, (req,res)=>{
	const data = {
		reqBody		  : req.body,
		authorization : auth.decode(req.headers.authorization)
	};
	cartController.addProductInCart(data).then(result => res.send(result));
});

//13.2.) Change product quantities
router.post("/updateQuantity/:productId", auth.verify, (req,res)=>{
	const data = {
		productId	  : req.params.productId,
		newQnty 	  : req.body.quantity,
		authorization : auth.decode(req.headers.authorization)
	};
	cartController.updProdQnty(data).then(result => res.send(result));
});

// 13.3.) Remove products from cart
router.patch("/removeFromCart/:productId", auth.verify, (req,res)=>{
	const data = {
		productId 	  : req.params.productId,
		authorization : auth.decode(req.headers.authorization)
	};
	cartController.rmvFromCart(data).then(result => res.send(result));
});

// 14.) Check-out Cart to Orders
router.put("/checkOut/:cartId", auth.verify, (req,res)=>{
	const data = {
		cartId		  : req.params.cartId,
		authorization : auth.decode(req.headers.authorization)
	};
	cartController.checkOutCart(data).then(result => res.send(result));
});

/*// 14.) Check-out Cart to Orders
router.put("/checkOut/:cartId", auth.verify, (req,res)=>{
	const data = {
		cartId		  : req.params.cartId,
		authorization : auth.decode(req.headers.authorization)
	};
	cartController.checkOutCart(data).then(result => res.send(result));
});
*/
// 15.) Check-out Single Product in Cart to Order 
router.patch("/checkOut/:cartId/:productId", auth.verify, (req,res)=>{
	const data = {
		cartId		  : req.params.cartId,
		productId	  : req.params.productId,
		authorization : auth.decode(req.headers.authorization)
	};
	cartController.checkOutSingleProduct(data).then(result => res.send(result));
});

// 16.) Update cart placeOrderOn to null value (admin only)
router.patch("/unPlaceOrder/:cartId", auth.verify, (req,res)=>{
	const data = {
		cartId	: req.params.cartId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	};
	cartController.updatePlaceOrderOn(data).then(result => res.send(result));
});

module.exports = router;
