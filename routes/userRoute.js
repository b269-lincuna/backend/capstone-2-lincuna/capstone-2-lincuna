const express = require("express");

// Creates a router instance that functions as middleware and routing system
const router = express.Router();

const userController = require("../controllers/userController");
const auth = require("../auth");

//route for checking email
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
});

// Route for retrieving user details
router.post("/updateUserDetails/:userId", auth.verify, (req, res) => {
	//const userData = auth.decode(req.headers.authorization);
	const data= {
		userId:req.params.userId,
		reqBody:req.body,
		authorization: auth.decode(req.headers.authorization)
	}
	userController.updateUserDetails(data).then(result => res.send(result));
});

// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile2({id: userData.id}).then(result => res.send(result));
});

// 1.) User registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(result => res.send(result));
});

// 2.) User authentication
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(result => res.send(result));
});

// 9.1) Retrieve User Details
router.get("/userDetails/:userId", auth.verify, (req,res)=>{
	const data = {
		userId	  	  : req.params.userId,
		authorization : auth.decode(req.headers.authorization)	
	}
	userController.getUserDetails(data).then(result => res.send(result));
});

// 9.2) Retrieve ALL User Details
router.get("/allUserDetails", auth.verify, (req,res)=>{
	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	userController.getAllUsers(isAdmin).then(result => res.send(result));
});

// 10.) [STRETCH GOAL] Set user as admin (Admin only) 
router.put("/:userId", auth.verify, (req,res)=>{
	const data = {
		userId  : req.params.userId,
		userRole: req.body.isAdmin,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	userController.updateUserRole(data).then(result => res.send(result));
});

// 11.) [STRETCH GOAL] Retrieve authenticated user’s orders
router.get("/showOrder/:userId", auth.verify, (req,res)=>{
	const data = {
		userId        : req.params.userId,
		authorization : auth.decode(req.headers.authorization)
	}
	userController.getUserOrder(data).then(result => res.send(result));
});

// 12.) Retrieve all orders (Admin only)
router.get("/showAllOrders", auth.verify, (req,res)=>{
	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	userController.showAllOrders(isAdmin).then(result => res.send(result));
});

module.exports = router;

